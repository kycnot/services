# KYCNOT.ME Services Discussions

## Please don't create any issues here! If you want to request a service listing use the [kycnot.me/request](https://kycnot.me/request) page!

This project is used for discussions about KYCNOT.ME projects.

Any feature request or ideas related to the project must go to the [website repo](https://gitlab.com/kycnot/kycnot.me).
